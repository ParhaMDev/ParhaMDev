<link rel="stylesheet" href="../css/style.css">
<footer class="footer d-flex">
    <div>
        <ul class="firts-col ">
            <h3 class="titr">وبلاگ</h3>
            <hr>
            <li class="form-list text-light item"><a class="form-link text-reset" href="#">تماس با ما</a></li>
            <li class="form-list text-light item"><a class="form-link text-reset" href="#">ارتباط با ما</a></li>
            <li class="form-list text-light item"><a class="form-link text-reset" href="#">مقالات</a></li>
        </ul>
    </div>
    <div>
        <ul class="second-col">
            <h3 class="titr">امکانات</h3>
            <hr>
            <li class="form-list text-light item" href="#"><a class="form-link text-reset" href="">دسته 1</a></li>
            <li class="form-list text-light item" href="#"><a class="form-link text-reset" href="">دسته 2</a></li>
            <li class="form-list text-light item" href="#"><a class="form-link text-reset" href="">دسته 3</a></li>
        </ul>
    </div>
    <p class="text-light">کلیه حقوق محتوا این سایت متعلق به وب سایت وبپروگ میباشد</p>

    <div class="bottom d-flex">
        <a href="www.instagram.com/parham__080" class="text-reset footer-link">parham__080
            <i class="fab fa-instagram"></i>
        </a>
        <a href="www.t.me/parhamdev" class="text-reset footer-link">ParhaMDev
            <i class="fab fa-telegram-plane"></i>
        </a>
        <p class="text-light footer-link">09353256665
            <i class="fas fa-mobile"></i>
        </p>
    </div>
</footer>


</body>

</html>