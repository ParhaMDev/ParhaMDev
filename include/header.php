<?php
include ("./include/config.php");
include ("./include/db.php");
$query = "SELECT * FROM categories";
$categories = $db->query($query);
?>
<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <title>ParhaMDev</title>
</head>
<body>
    <navbar class="nav">
        <div>
        <ul class="items">
                    <?php
                    if ($categories->rowCount() > 0) {
                        foreach ($categories as $category) {
                            ?>
                            <li class="nav-item <?php echo ( isset($_GET['category']) && $category['id'] == $_GET['category']) ? "active" : ""; ?> ">
                                <a class="nav-link" href="index.php?category=<?php echo $category['id'] ?>"> <?php echo $category['title'] ?> </a>
                            </li>
                    <?php
                        }
                    }
                    ?>
                </ul>

        </div>  
        <a class="text-reset text-decoration-none"href="www.ParhaMDev.ir"><h1 class="domain"><span class="logo">ParhaMDev</span>.ir</h1></a>
    </navbar>