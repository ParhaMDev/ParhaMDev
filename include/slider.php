<link rel="stylesheet" href="../css/style.css">
<?php
$query_slider = "SELECT * FROM posts_slider";

$posts_slider = $db->query($query_slider);

?>
<slidebar>
    <div class="zoom">
        <div id="slider" class="carousel slide carousel-fade mb-5" data-ride="carousel">
            <ol class="carousel-indicators">
                <li class="active" data-target="#slider" data-slide-to="0"></li>
                <li data-target="#slider" data-slide-to="1"></li>
                <li data-target="#slider" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
            <?php

            if ($posts_slider->rowCount() > 0) {

                foreach ($posts_slider as $post_slider) {
                    $id_post = $post_slider['post_id'];
                    $query_posts = " SELECT * FROM posts WHERE id=$id_post ";

                    $post = $db->query($query_posts)->fetch();

                    ?>

                    <div class="carousel-item <?php echo ( $post_slider['active'] ) ? "active" : ""; ?>">

                        <img src="./upload/posts/<?php echo $post['image'] ?>" class="img-fluid d-block " alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5><?php echo $post['title'] ?></h5>
                            <p>
                                <?php echo substr( $post['body'] ,0, 200). "..." ?>
                            </p>
                            <a href="single.php?post=<?php echo $post['id'] ?>" class="btn btn-danger"> مشاهده </a>
                        </div>
                    </div>

            <?php

                }
            }
            ?>

        </div>

        <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</slidebar>